var express = require('express');
var router = express.Router();
var numClientes;
var Cliente;
var aux;
var resultado;
var Itens;

router.get('/', function(req, res, next) {
 
  res.render('index', { title: 'Node Express API', message: 'Matheus Frantz de Faria.'});
  
});

var http = require('http');
 
Array.prototype.findByValueOfObject = function(key, value) {
  return this.filter(function(item) {
    return (item[key] === value);
  });
}



http.get('http://www.mocky.io/v2/598b16291100004705515ec5', (resp) => {

    let dados = '';

    // A chunk of data has been recieved. Exemplo Padrao Globo
    resp.on('data', (chunk) => {
      dados += chunk;
 
    }); 
 
    // Aguardando fim do recebimento das informacoes.
    resp.on('end', () => {
      
      // Transforma os dados dos clientes em uma estrutura
      Cliente =  JSON.parse(dados);

      var contadorClientes = 0;
      
      Cliente.forEach(function(obj) {

        contadorClientes++;

      });

    numClientes=contadorClientes;


  });}).on("error", (err) => {

    console.log("Erro: " + err.message);

  }

);

var estruturaValoresGastos = '';
http.get('http://www.mocky.io/v2/598b16861100004905515ec7', (resp) => {

  let dados = '';
  resp.on('data', (chunk) => {

    dados += chunk;

  }); 

  resp.on('end', () => {

    var Vendas =  JSON.parse(dados);
    var contadorVendas;
    var cpf;
    var somaValores = 0;
    var idClienteMaiorCompra = 0;
    var valorClienteMaiorCompra = 0;
    var totalVendasCliente;
    var ano;

    contadorClientes = 0;
    estruturaValoresGastos+='[';

    while(contadorClientes<numClientes){

      contadorVendas = 0;
      somaValores=0;
      totalVendasCliente=0;

      Vendas.forEach(function(obj) {

        cpf = JSON.stringify(Cliente[contadorClientes].cpf);

        /// Tratando formato de cpf de xxx.xxx.xxx-xx para 0xxx.xxx.xxx.xx 
        cpf='0'+cpf.slice(1,4)+'.'+cpf.slice(5,8)+'.'+cpf.slice(9,12)+'.'+cpf.slice(13,15);
        
        if (Vendas[contadorVendas].cliente == cpf){

          somaValores += Vendas[contadorVendas].valorTotal;
          totalVendasCliente++;
          ano=Vendas[contadorVendas].data;
          ano=ano.slice(6,10);

          if(Vendas[contadorVendas].valorTotal>valorClienteMaiorCompra && ano==2016){
            
            valorClienteMaiorCompra = Vendas[contadorVendas].valorTotal;
            idClienteMaiorCompra = contadorClientes;

          }

        }

        contadorVendas++;
        
      });

      if(contadorClientes<numClientes-1){

        estruturaValoresGastos += '{"nome": "' + Cliente[contadorClientes].nome + '","valorTotal": ' + somaValores +',"vendasCliente": '+totalVendasCliente + '},';

      }else{

        estruturaValoresGastos += '{"nome": "' + Cliente[contadorClientes].nome + '","valorTotal": ' + somaValores +',"vendasCliente": ' + totalVendasCliente + '}';

      }

      contadorClientes++;

    }

    estruturaValoresGastos += ']';
    estruturaValoresGastos = JSON.parse(estruturaValoresGastos);
    resultado = estruturaValoresGastos.sort(function(a, b){return a.valorTotal - b.valorTotal});
    resultado = resultado.reverse();

    console.log("\nResultados por ordem de maior valor de compra: \n"); 
    
    var maiorNumeroCompras = 0;
    var idmaiorNumeroCompras = 0;

    aux = 0;
    resultado.forEach(function(obj) {
    
      console.log(resultado[aux].nome + " -- Valor Gasto $: " +resultado[aux].valorTotal + " ");
      if(maiorNumeroCompras<resultado[aux].totalVendasCliente){
        maiorNumeroCompras=resultado[aux].totalVendasCliente;
        idmaiorNumeroCompras = aux;
      }


      aux++;

    });

    console.log('\nCliente de maior compra no ano de 2016 é ' + Cliente[idClienteMaiorCompra].nome + ' no valor de ' + valorClienteMaiorCompra + ' $ \n');


    ///cliente mais fiel é considerado o cliente que fez o maior número de compras, nao de itens ou valor total
    console.log("\nClientes mais fieis: \n"); 
    
    aux = 0;
    var maiorNumeroCompras = 0;
    var idmaiorNumeroCompras = 0;

    resultado = estruturaValoresGastos.sort(function(a, b){return a.vendasCliente - b.vendasCliente});
    
    resultado = resultado.reverse();
    
    resultado.forEach(function(obj) {
      
      console.log(resultado[aux].nome + " -- Qtd Compras: " + resultado[aux].vendasCliente + " ");

      aux++;

    });

    ///A recomendacao de vinho é definida pela safra mais antiga.

    ///entre com cpf do cliente
    console.log("\nDefinindo vinho \n");
    
    //var cpfDigitado =  '"000.000.000-05"'; caso a pergunta sobre o cpf nao funcione, descomentar essa linha 
                                          ////////////////////e comentar a pergunta em seu final na linha 240

    process.stdin.resume();
    var readline = require('readline');
    var resp = "";
 
    console.log("Digite o cpf para recomendacao de vinho no formato xxx.xxx.xxx-xx: \n");

    var stdin = process.openStdin();

    stdin.addListener("data", function(d) {
  
    cpfDigitado = '"'+d.toString().trim()+'"';


    var safraMaisAntiga = 3000;
    var recomendacaoVinho;
    var nomeVinho;
    var origemVinho;
    
 
    contadorVendas = 0;
    cpf = cpfDigitado;

    Vendas.forEach(function(obj) {

      /// Tratando formato de cpf de xxx.xxx.xxx-xx para 0xxx.xxx.xxx.xx 
      cpf='0'+cpf.slice(1,4)+'.'+cpf.slice(5,8)+'.'+cpf.slice(9,12)+'.'+cpf.slice(13,15);
      
      if (Vendas[contadorVendas].cliente == cpf){

        contadorItens = 0;
  
        Vendas[contadorVendas].itens.forEach(function(obj) {

          if(Vendas[contadorVendas].itens[contadorItens].safra < safraMaisAntiga){

            safraMaisAntiga=Vendas[contadorVendas].itens[contadorItens].safra;
            nomeVinho = Vendas[contadorVendas].itens[contadorItens].produto;
            origemVinho = Vendas[contadorVendas].itens[contadorItens].pais;
            recomendacaoVinho = "O vinho recomendado pela API é o "+ nomeVinho + ". \nOrigem: " + origemVinho + ". \nSafra de " + safraMaisAntiga + ".\n";

          }

          contadorItens++;

        });

      }

      contadorVendas++;
      
    });

    console.log(recomendacaoVinho);
    
  
  }); /// fim pergunta pelo cpf

    
  });


}).on("error", (err) => {
    
    console.log("Error: " + err.message);
    
  });
  



module.exports = router;

